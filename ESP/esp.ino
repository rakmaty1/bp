#include <Arduino.h>
#include <ESP8266WiFi.h>

#define UPD_AVAIL 1
#define UPD_NO 0
#define UPD_FAIL -1

#define SERVER_IP "10.0.0.36"
#define SERVER_PORT 2999

//the certificate of the update server
const char PROGMEM server_cert[] = R"EOF(
-----BEGIN CERTIFICATE-----
MIICSzCCAfGgAwIBAgIUKHncRbWYSwonD5hBZxF+ihJcxfMwCgYIKoZIzj0EAwIw
ezELMAkGA1UEBhMCQ1oxEzARBgNVBAgMClNvbWUtU3RhdGUxHTAbBgNVBAoMFEJh
Y2hlbG9yIFRoZXNpcyB0ZXN0MRMwEQYDVQQDDApNYXR5YXMgUmFrMSMwIQYJKoZI
hvcNAQkBFhRyYWttYXR5MUBmaXQuY3Z1dC5jejAeFw0yMjA2MjEyMzQ2NTFaFw00
OTExMDUyMzQ2NTFaMHsxCzAJBgNVBAYTAkNaMRMwEQYDVQQIDApTb21lLVN0YXRl
MR0wGwYDVQQKDBRCYWNoZWxvciBUaGVzaXMgdGVzdDETMBEGA1UEAwwKTWF0eWFz
IFJhazEjMCEGCSqGSIb3DQEJARYUcmFrbWF0eTFAZml0LmN2dXQuY3owWTATBgcq
hkjOPQIBBggqhkjOPQMBBwNCAARLA+sT0uTTynjMX7FbqRB1l9JxXmWF+LfFE4Xv
x2UU7xKE4dAOEVeGfYiYtfZY/C+GYZKS1l8Xnl+iqwCbolRUo1MwUTAdBgNVHQ4E
FgQUthzwywHuO+54MvRWCSAnQVbJPTcwHwYDVR0jBBgwFoAUthzwywHuO+54MvRW
CSAnQVbJPTcwDwYDVR0TAQH/BAUwAwEB/zAKBggqhkjOPQQDAgNIADBFAiEAsVFe
DjtR0E9MhqNDCaERRaZ41EvqF6vvcYpWA3g/KOECIHa+R/zBGcEB8Ix1yvyeJBv4
uBf+oOl8LMOOQQOkYPMi
-----END CERTIFICATE-----
)EOF";

//The list of certificates to be used with SSL connection
X509List server_cert_list(server_cert);

/**
 * Arduino setup function, called once on startup, starts the Serial interface
 */
void setup()
{
  Serial.begin(115200);
}

/**
 * Connects the ESP8266 to wifi with given parameters
 * ssid - the ssid of the wifi network
 * password - the password of the wifi network
 * 
 * returns true on successful connection, false otherwise
 */
bool connectToWifi(const String& ssid, const String& password)
{
  WiFi.begin(ssid, password);

  if (WiFi.waitForConnectResult(10000) == WL_CONNECTED)
    return true;
  else
    return false;
}

/**
 * Disconnects the ESP8266 from wifi
 */
void disconnectFromWifi()
{
  WiFi.disconnect();
}

/**
 * Checks an answer from the Arduino when programming.
 * returns true if Arduino respods accordingly (with bytes 0x14 and 0x10), falsr otherwise
 */
bool checkAnswer()
{
  //wait for something
  while (!Serial.available())
    continue;
  int a = Serial.read();
  //wait for something
  while (!Serial.available())
    continue;
  int b = Serial.read();
  return a == 0x14 && b == 0x10;
}

/**
 * Called from performUpdate, it actually uploads the firmware.
 * For each page (128 bytes), it sends the page number, waits for response and then uploads that page.
 * The last page is padded with 0xFF to the full 128 bytes
 * data - pointer to the downloaded firmware to be uploaded to Arduino
 * len - length of the firmware
 * 
 * returns - true on success, false on failure
 */
bool uploadFirmware(byte* data, size_t len)
{
  int pages = len / 0x80;
  int pageNum = 0;
  byte* currentPage = data;
  while (pages >= 0)
  {
    //write current page number
    byte addr[4];
    addr[0] = 0x55;
    addr[1] = pageNum & 0xFF;
    addr[2] = (pageNum >> 8) & 0xFF;
    addr[3] = 0x20;
    Serial.write(addr, 4);
    
    if (!checkAnswer())
      return false;
    
    //write current page
    byte msg[133];
    msg[0] = 0x64;
    msg[1] = 0x00;
    msg[2] = 0x80;
    msg[3] = 0x46;
    //last page, need to add padding with 0xFF
    if (pages == 0)
    {
      byte lastPage[0x80];
      memset(lastPage, 0xFF, 0x80);
      memcpy(lastPage, currentPage, len % 0x80);
      currentPage = lastPage;
    }
    memcpy(msg + 4, currentPage, 0x80);
    msg[132] = 0x20;
    Serial.write(msg, 133);
    if (!checkAnswer())
      return false;

    pageNum += 0x40;
    currentPage += 0x80;
    pages--;
  }
  return true;
}

/**
 * Performs a firmware upload to the Arduino
 * At first, it initializes the STK500 protocol, enters in programming mode
 * Then it uploads the given firmware, after which it leaves he programming mode.
 * The Arduino needs to reset itself before this function is called.
 * data - pointer to the downloaded firmware to be uploaded to Arduino
 * len - length of the firmware
 * 
 * returns - true on success, false on failure
 */
bool performUpdate(byte* data, size_t len)
{
  delay(150);
  Serial.write("\x30\x20");
  delay(10);
  while (Serial.available() > 0)
    Serial.read();
  Serial.write("\x30\x20");
  delay(10);
  while (Serial.available() > 0)
    Serial.read();
    
  //stk500 initialize
  Serial.write("\x30\x20");
  if (!checkAnswer())
    return false;
    
  //enter programming mode
  Serial.write("\x50\x20");
  if (!checkAnswer())
    return false;
    
  if (!uploadFirmware(data, len))
    return false;
    
  //leave programming mode
  Serial.write("\x51\x20");
  if (!checkAnswer())
    return false;
  return true;
}

/**
 * returns the value of given hexadicimal digit or -1 if given character is none of 0-9, a-f, A-F
 */
byte hexCharVal(char c)
{
  if (c >= '0' && c <= '9')
    return c - '0';
  else if (c >= 'A' && c <= 'F')
    return c - 'A' + 10;
  else if (c >= 'a' && c <= 'f')
    return c - 'a' + 10;
  else return -1;
}

/**
 * parses the byte written hexadecimaly as two ascii characters c1 and c2
 * return - the byte parsed
 */
byte toByte(char c1, char c2)
{
  return (hexCharVal(c1) << 4) + hexCharVal(c2);
}

/**
 * Parses one line of the hex file received. Also checks the checksum of this line.
 * line - the line of the hex file to parse
 * rawData - pointer to the memory where this line should be stored
 * 
 * return - length of the bytes parsed from this particular line
 *          0 - this line does not contain data
 *          -1 on failure (line does not start with :)
 *          -2 on failure (the checksum of given line is invalid)
 */
int parseHexFileLine(const String& line, byte* rawData)
{
  if (line[0] != ':')
    return -1;
    
  byte len = toByte(line[1], line[2]);
  byte type = toByte(line[7], line[8]);

  if (type != 0)
    return 0;

  int i = 9;
  for (int j = 0; j < len; i += 2, j++)
    rawData[j] = toByte(line[i], line[i + 1]);

  //check checksum
  byte checksum = toByte(line[i], line[i + 1]);
  
  long sum = len + toByte(line[3], line[4]) + toByte(line[5], line[6]) + type + checksum;
  for (int i = 0; i < len; i++)
    sum += rawData[i];

  if ((sum & 0xFF) != 0)
    return -2;

  return len;
}

/**
 * Parses the hex file of firmware received from the server
 * file - the file received
 * parsedLen - output parameter, the length of the parsed file in bytes
 * 
 * returns - pointer to the parsed hex file, caler should free this memory after usage (with delete[])
 *           nullptr on failure
 */
byte* parseHexFile(const String& file, size_t* parsedLen)
{
  int from = 0;
  int to = file.indexOf("\r\n");

  //each byte in hex is represented as two chars, so this is still more than needed
  byte* decodedBytes = new byte[file.length() / 2];
  int len = 0;
  
  while (true)
  {
    String line = file.substring(from, to);
    int lineLen = parseHexFileLine(line, decodedBytes + len);
    if (lineLen == -1 || lineLen == -2)
    {
      delete[] decodedBytes;
      return nullptr;
    }
    len += lineLen;
    from = to + 2; //skip crlf
    to = file.indexOf("\r\n", from);
    if (to == -1)
      break;
  }

  *parsedLen = len;
  return decodedBytes;
}

/**
 * Sets current time via NTP protocol, so a SSL certificate can be validated
 */
void setTime()
{
  configTime(0, 0, "pool.ntp.org", "time.nist.gov", "time.windows.com");

  time_t now = time(nullptr);
  while (now < 8 * 3600 * 2)
  {
    delay(500);
    now = time(nullptr);
  }
}

/**
 * Connects to the update server and tries to download an firmware update of the given firmware name
 * Should be called only after checking that an update is available
 * fwName - the name of firmware to update
 * fwVersion - the currently installed version of that firmware
 * len - output parameter, will contain the length of the firmware downloaded in bytes
 * 
 * returns - pointer to the downloaded firmware, caller should free this memory after usage (with delete[])
 *           nullptr on failure
 */
byte* downloadUpdate(const String& fwName, const String& fwVersion, size_t* len)
{
  setTime();
  WiFiClientSecure client;
  client.setTrustAnchors(&server_cert_list);
  IPAddress serverIP;
  serverIP.fromString(SERVER_IP);
  
  if (client.connect(serverIP, SERVER_PORT))
  {
    client.println("DOWNLOAD");
    client.println(fwName);
    client.println(fwVersion);
    size_t filesize = 0;
    filesize = client.parseInt();
    
    if (filesize == 0)
    {
      client.stop();
      return nullptr;
    }
    
    //skip CRLF
    client.read();
    client.read();

    char* data = new char[filesize];
    client.readBytes(data, filesize);
    client.stop();

    String dataStr(data);
    delete[] data;
    byte* parsedData = parseHexFile(dataStr, len);
    
    return parsedData;
  }
  else
    return nullptr;
}

/**
 * Connects to the update server and checks if firmware update of the given firmware name is available
 * fwName - the name of firmware to check
 * fwVersion - the currently installed version of that firmware
 * 
 * returns UPD_AVAIL if firmware update is available
 *         UPD_NO if the current formware is the latest
 *         UPD_FAIL if the check fails
 */
int checkForUpdate(const String& fwName, const String& fwVersion)
{
  setTime();
  WiFiClientSecure client;
  client.setTrustAnchors(&server_cert_list);
  IPAddress serverIP;
  serverIP.fromString(SERVER_IP);

  if (client.connect(serverIP, SERVER_PORT))
  {
    client.println("CHECK");
    client.println(fwName);
    client.println(fwVersion);

    String answer = client.readStringUntil('\n');
    client.stop();
    
    if (answer.endsWith("\r"))
      answer.remove(answer.length() - 1);
    if (answer.equals("UPDATE AVAILABLE"))
      return UPD_AVAIL;
    else
      return UPD_NO;
  }
  else
    return UPD_FAIL;
}

/**
 * Read one line of text from the Serial interface
 * return the line read as String
 */
String readLine()
{
  String line = Serial.readStringUntil('\n');
  if (line.endsWith("\r"))
    line.remove(line.length() - 1);
  return line;
}

//if true, indicates that new firmware is ready to be installed on the Arduino
bool newFirmwareReady = false;
//buffer containing the new firmware to be installed
byte* newFirmware = nullptr;
//size of the new firmware
size_t newFirmwareLen = 0;

/**
 * Arduino loop function, is called in endless loop while the device is running
 * It implements the communication logic with Arduino
 */
void loop()
{
  if (Serial.available() > 0)
  {
    String text = readLine();
    
    if (text.equals("CONNECT"))
    {
      String ssid = readLine();
      String password = readLine();
      if (connectToWifi(ssid, password))
        Serial.println("CONNECTED");
      else
        Serial.println("FAIL");
    }
    else if (text.equals("DISCONNECT"))
    {
      disconnectFromWifi();
      Serial.println("DISCONNECTED");
    }
    else if (text.equals("CHECK UPDATE"))
    {
      String fwName = readLine();
      String fwVersion = readLine();
      int i = checkForUpdate(fwName, fwVersion);
      if (i == UPD_AVAIL)
      {
        if (newFirmware != nullptr)
        {
          delete[] newFirmware;
          newFirmware = nullptr;
          newFirmwareLen = 0;
          newFirmwareReady = false;
        }
        newFirmware = downloadUpdate(fwName, fwVersion, &newFirmwareLen);
        if (newFirmware != nullptr)
        {
          Serial.println("UPDATE AVAILABLE");
          newFirmwareReady = true;
        }
        else
          Serial.println("FAIL");
      }
      else if (UPD_NO)
        Serial.println("NO UPDATE");
      else
        Serial.println("FAIL");
    }
    else if (text.equals("PERFORM UPDATE"))
    {
      if (newFirmwareReady)
      {
        performUpdate(newFirmware, newFirmwareLen);
        delete[] newFirmware;
        newFirmware = nullptr;
        newFirmwareLen = 0;
        newFirmwareReady = false;
      }
      else
        Serial.println("FIRMWARE NOT DOWNLOADED");
    }
    else
    {
      Serial.println("UNKNOWN COMMAND");
    }
  }
}
