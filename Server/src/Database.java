
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



/**
 *
 * @author Matyáš Rak
 */
public class Database 
{
	public static final String DB_FILENAME = "database.sqlite";
	
	public static boolean checkDb()
	{
		File file = new File(DB_FILENAME);
		return file.exists();
	}
	
	public static void create()
	{
		String url = "jdbc:sqlite:" + DB_FILENAME;
		String createSQL = 
				"CREATE TABLE IF NOT EXISTS firmware ("
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "name TEXT NOT NULL,"
				+ "version INTEGER NOT NULL,"
				+ "filepath TEXT NOT NULL,"
				+ "UNIQUE(name, version))";
		
		try (Connection conn = DriverManager.getConnection(url))
		{
			try (Statement stmt = conn.createStatement())
			{
				stmt.executeUpdate(createSQL);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(System.err);
		}
	}
	
	public static int getNewestFirmwareVersion(String firmwareName)
	{
		String url = "jdbc:sqlite:" + DB_FILENAME;
		String sql = 
				"SELECT a.version FROM firmware a"
				+ " INNER JOIN (SELECT name, MAX(version) version FROM firmware WHERE name = ? GROUP BY name) b"
				+ " ON a.name = b.name AND a.version = b.version";
		try (Connection conn = DriverManager.getConnection(url))
		{
			try (PreparedStatement stmt = conn.prepareStatement(sql))
			{
				stmt.setString(1, firmwareName);
				try (ResultSet rs = stmt.executeQuery())
				{
					if (rs.next())
						return rs.getInt(1);
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(System.err);
		}
		return -1;
	}
	
	public static String getFirmwarePath(String firmwareName, int firmwareVersion)
	{
		String url = "jdbc:sqlite:" + DB_FILENAME;
		String sql = "SELECT filepath FROM firmware WHERE name = ? AND version = ?";
		try (Connection conn = DriverManager.getConnection(url))
		{
			try (PreparedStatement stmt = conn.prepareStatement(sql))
			{
				stmt.setString(1, firmwareName);
				stmt.setInt(2, firmwareVersion);
				try (ResultSet rs = stmt.executeQuery())
				{
					if (rs.next())
						return rs.getString(1);
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(System.err);
		}
		return null;
	}
	
	
	public static void addNewFirmware(String firmwareName, int firmwareVersion, String filepath)
	{
		String url = "jdbc:sqlite:" + DB_FILENAME;
		String sql = "INSERT INTO firmware (name, version, filepath) VALUES (?, ?, ?)";
		try (Connection conn = DriverManager.getConnection(url))
		{
			try (PreparedStatement stmt = conn.prepareStatement(sql))
			{
				stmt.setString(1, firmwareName);
				stmt.setInt(2, firmwareVersion);
				stmt.setString(3, filepath);
				stmt.executeUpdate();
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(System.err);
		}
	}
}
