
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Scanner;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;


/**
 *
 * @author Matyáš Rak
 */
public class Server
{
	public static final String KEYSTORE = "keystore.p12";
	
	public static void main(String[] args)
	{
		if (!Database.checkDb())
		{
			System.out.println("Database file not found, creating new");
			Database.create();
		}
		
		if (args.length >= 1)
		{
			if (args[0].equalsIgnoreCase("start"))
				startServer();
			else if (args[0].equalsIgnoreCase("add") && args.length >= 4)
			{
				try
				{
					String name = args[1];
					int version = Integer.parseInt(args[2]);
					String path = args[3];
					String test = Database.getFirmwarePath(name, version);
					if (test != null)
						System.out.println("Firmware of this name and version already exists");
					else
						Database.addNewFirmware(name, version, path);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Firmware version must be an positive integer");
				}
			}
			else
				printHelp();
		}
		else
			printHelp();
	}
	
	public static void printHelp()
	{
		System.out.println("Usage:");
		System.out.println("To start OTA server use: java -jar OTA_server.jar start");
		System.out.println("To add new firmware use: java -jar OTA_server.jar add <firmware_name> <firmware_version> <path_to_firmware>");
		System.out.println("Firmware version must be an positive integer");
	}
	
	public static void startServer()
	{
		if (!Database.checkDb())
			Database.create();
		
		int port = 2999;
		
		System.out.println("Starting Arduino OTA server at port " + port);
		try
		{
			KeyStore store = KeyStore.getInstance("PKCS12");
			char[] storePass = "testbp1234".toCharArray();
			store.load(new FileInputStream(KEYSTORE), storePass);
			
			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(store, storePass);

			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(kmf.getKeyManagers(), null, null);
			
			SSLServerSocketFactory factory = ctx.getServerSocketFactory();

			try (ServerSocket listener = factory.createServerSocket(port))
			{
				SSLServerSocket sslListener = (SSLServerSocket) listener;
				sslListener.setEnabledProtocols(new String[] {"TLSv1.2"});
				while (true)
				{
					Socket client = sslListener.accept();
					System.out.println("Client connected at port " + client.getPort());
					new Thread(() ->
					{
						try (PrintWriter out = new PrintWriter(client.getOutputStream(), true);
								BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream())))
						{
							client.setSoTimeout(5000);
							String command = in.readLine();
							String firmwareName = in.readLine();
							int firmwareVersion = Integer.parseInt(in.readLine());

							System.out.println("Command: " + command);
							System.out.println("Firmware name: " + firmwareName);
							System.out.println("Firmware version: " + firmwareVersion);

							if (command.equals("CHECK"))
							{
								int version = Database.getNewestFirmwareVersion(firmwareName);
								if (version == -1)
								{
									System.out.println("Unknown firmware name");
									out.println("FAIL");
								}
								else if (version > firmwareVersion)
								{
									System.out.println("Firmware update available");
									out.println("UPDATE AVAILABLE");
								}
								else
								{
									System.out.println("Client has latest firmware");
									out.println("NO UPDATE");
								}
							}
							else if (command.equals("DOWNLOAD"))
							{
								int version = Database.getNewestFirmwareVersion(firmwareName);
								if (version > firmwareVersion)
								{
									System.out.println("Sending new firmware");

									String path = Database.getFirmwarePath(firmwareName, version);
									System.out.println(path);

									File file = new File(path);
									out.println(file.length());
									Scanner s = new Scanner(file);
									while(s.hasNextLine())
										out.println(s.nextLine());
								}
								else
								{
									System.out.println("Unknown firmware name or update not available");
									out.println("FAIL");
								}
							}
							else
							{
								out.println("FAIL");
								System.out.println("Invalid command, closing connection.");
							}
							client.close();
						}
						catch (SocketTimeoutException e)
						{
							System.out.println("Client timeout, closing connection.");
						}
						catch (IOException e)
						{
							e.printStackTrace(System.err);
						}
					}).start();
				}
			}
			catch (IOException e)
			{
				e.printStackTrace(System.err);
			}
		}
		catch (KeyStoreException | NoSuchAlgorithmException | KeyManagementException | UnrecoverableKeyException | CertificateException | IOException e)
		{
			e.printStackTrace(System.err);
		}
	}
}
