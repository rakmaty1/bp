#define FIRMWARE_NAME "BP-test"
#define FIRMWARE_VERSION 3

#define WIFI_NAME "WI-FI"
#define WIFI_PASSWORD "**********"

const int MAX_WIFI_CONNECTION_ATTEMPTS = 5;
const int MAX_UPDATE_CHECK_ATTEMPTS = 5;

const int RESET_PIN = 12;

void clearRXBuffer()
{
  while (Serial.available() > 0)
    Serial.read();
}

void setup()
{
  digitalWrite(RESET_PIN, HIGH);
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RESET_PIN, OUTPUT);

  delay(1000);
  clearRXBuffer();
}

void reset()
{
  digitalWrite(RESET_PIN, LOW);
  for(;;);
}

unsigned long ledPrevTime = 0;
bool ledOn = false;
const int LED_INTERVAL = 2000;

void blinkLed()
{
  unsigned long ledCurrTime = millis();
  if (ledCurrTime - ledPrevTime > LED_INTERVAL)
  {
    ledPrevTime = ledCurrTime;
    if (ledOn)
    {
      digitalWrite(LED_BUILTIN, LOW);
      ledOn = false;
    }
    else
    {
      digitalWrite(LED_BUILTIN, HIGH);
      ledOn = true;
    }
  }
}

unsigned long updPrevTime = 0;
const unsigned long UPDATE_INTERVAL = 60000; //1 hour
bool timeForUpdate = true; //check for update on reboot

bool wifiConnecting = false;
bool wifiConnected = false;
bool checkingUpdate = false;
int connectionAttempts = 0;

String line = "";

bool readLine(String& str)
{
  while(Serial.available() > 0)
  {
    char c = Serial.read();
    if (c == '\n')
    {
      if (str.endsWith("\r"))
        str.remove(str.length() - 1);
      return true;
    }
    str.concat(c);
  }
  return false;
}

bool connectToWifi()
{
  if (!wifiConnecting)
  {
    clearRXBuffer();
    Serial.println("CONNECT");
    Serial.println(WIFI_NAME);
    Serial.println(WIFI_PASSWORD);
    wifiConnecting = true;
    connectionAttempts = 1;
  }
  else //wifiConnecting
  {
    if (readLine(line))
    {
      if (line.equals("CONNECTED"))
      {
        wifiConnecting = false;
        wifiConnected = true;
      }
      else //fail
      {
        connectionAttempts++;
        if (connectionAttempts > MAX_WIFI_CONNECTION_ATTEMPTS)
        {
          wifiConnecting = false;
          line = "";
          return false;
        }
        else
        {
          Serial.println("CONNECT");
          Serial.println(WIFI_NAME);
          Serial.println(WIFI_PASSWORD);
        }
      }
      line = "";
    }
  }
  return true;
}

void disconnectFromWifi()
{
  Serial.println("DISCONNECT");
  wifiConnected = false;
}

int updateAttempts = 0;

void checkUpdate()
{
  if (!wifiConnected)
  {
    if (!connectToWifi())
      timeForUpdate = false;
  }
  else //wifiConnected
  {
    if (!checkingUpdate)
    {
      clearRXBuffer();
      Serial.println("CHECK UPDATE");
      Serial.println(FIRMWARE_NAME);
      Serial.println(FIRMWARE_VERSION);
      checkingUpdate = true;
      updateAttempts = 1;
    }
    else //checkingUpdate
    {
      if (readLine(line))
      {
        if (line.equals("UPDATE AVAILABLE"))
        {
          Serial.println("PERFORM UPDATE");
          delay(100);
          reset();
        }
        else if (line.equals("NO UPDATE"))
        {
          checkingUpdate = false;
          timeForUpdate = false;
          disconnectFromWifi();
        }
        else //fail
        {
          updateAttempts++;
          if (updateAttempts > MAX_UPDATE_CHECK_ATTEMPTS)
          {
            checkingUpdate = false;
            timeForUpdate = false;
            disconnectFromWifi();
          }
          else
          {
            Serial.println("CHECK UPDATE");
            Serial.println(FIRMWARE_NAME);
            Serial.println(FIRMWARE_VERSION);
          }
        }
        line = "";
      }
    }
  }
}

void checkTimeForUpdate()
{
  if (!timeForUpdate)
  {
    unsigned long updCurrTime = millis();
    if (updCurrTime - updPrevTime > UPDATE_INTERVAL)
      timeForUpdate = true;
  }
}

void loop()
{
  blinkLed();
  checkTimeForUpdate();
  if (timeForUpdate)
    checkUpdate();
}
